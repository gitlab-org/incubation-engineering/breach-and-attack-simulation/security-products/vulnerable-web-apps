const server = require("http").createServer((req, res) => {
  const codeToRun = new URL(`http://${req.headers.host}${req.url}`).searchParams.get('run_code');

  if (!codeToRun) {
    res.writeHead(200);
    res.end(`<html><head><meta charset="UTF-8"></head><body>Isn't math great? Did you know <a href="/?run_code=1%2b1"><code>1 + 1 = 2</code></a></body></html>`);
    return;
  }

  try {
    console.log(typeof codeToRun);
    console.log(codeToRun);
    const result = eval(`${codeToRun}`);
    res.writeHead(200);
    res.end(`<html><head><meta charset="UTF-8"></head><body><div>Processed run_code returned:<br/><pre>${result}</pre></div></body></html>`);
  } catch (err) {
    console.error("error running code", err);
    res.writeHead(500);
    res.end(`<html><head><meta charset="UTF-8"></head><body><div>Unable to process run_code.</div></body></html>`);
  }
});

server.listen(80, '0.0.0.0', () => console.log("server has started"));

process.on('SIGINT', () => {
  server.close();
  process.exit();
});
